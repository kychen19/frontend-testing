"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class material extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const service = sequelize.define("service");
      material.hasMany(service);
      material.belongsTo(models.subject, { foreignKey: "subjectId" });
    }
  }
  material.init(
    {
      name: DataTypes.STRING,
      title: DataTypes.STRING,
      description: DataTypes.STRING,
      subjectId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "material",
    }
  );
  return material;
};
