const { user } = require("../models");
const bcrypt = require("bcrypt");
var passport = require("passport");
const { render } = require("ejs");
const { use } = require("passport");
const role = {
  ORANGTUA: "ORANGTUA",
  TUTOR: "TUTOR",
};
class Auth {
  static register(req, res) {
    console.log(req.body);

    const email = req.body.email;
    const username = req.body.username;
    const pw1 = req.body.password;
    const password_confirm = req.body.password_confirm;
    const role = req.body.role;

    if (password_confirm != pw1) {
      res.status(500).json({
        status: 'password tidak sama',
        message,
      });
    } else {
      const password = bcrypt.hashSync(pw1, 10);
      user
        .create({
          email: email,
          username: username,
          password: password,
          role: role,
        })
        .then((user) => {
          // console.log(user);
          // res.redirect("/login");
          res.status(201).json({
            status: 'account created',
            data : user,
          });
        
        })
        .catch((err) => {
          res.status(500).json({
            status: 'error',
            message,
          });
        
          // res.render("register");
        });
    }
  }
  static getregister(req, res) {
    // res.render("register");
    res.json({
      'status': 200,
      'message': 'found!',
      'data' : res
  })

  }

  static getHomepage(req, res) {
    res.render("homepage");
  }

  static login(req, res) {
    console.log("login controller");
    const mail = req.body.email;
    user
      .findOne({ attributes: ["role"], where: { email: mail } })
      .then((response) => {
        const r = '{"role":"ORANGTUA"}';
        if (JSON.stringify(response) == r) {
          res.json({
            'status': 200,
            'message': 'login success (orangtua account)',
            'data' :response
        })
        } else {
          res.json({
            'status': 200,
            'message': 'login success (tutor account!)',
            'data' :response
          })
        }
      })
      .catch((e) => {
        res.json({
          status: 422,
          message: "Error get Tutor",
        });
      });
  }
  static getLoginPage(req, res) {
    // res.render("login");
    res.json({
      'status': 200,
      'message': 'Login success!',
      'data' :res
    })
  }
  static getdashboard(req, res) {
    res.render("homepage");
  }
  static getregister(req, res) {
    res.render("register");
  }

  static getHomepage(req, res) {
    res.render("homepage");
  }
  static getDashboardTutor(req, res) {
    res.render("dashboardTutor");
  }
  static getDetailMurid(req, res) {
    res.render("detailMurid");
  }
  static getDetailLaporan(req, res) {
    res.render("detailLaporan");
  }
  static getTambahLaporan(req, res) {
    res.render("tambahLaporan");
  }
  // static getTutorPenawaran(req, res) {
  //   res.render("tutor_penawaran");
  // }

  static getParentsDashboard(req, res) {
    res.render("pages/dashboard-orang-tua.ejs");
  }

  static getBehaviouralDataReport(req, res) {
    res.render("pages/detail-laporan.ejs");
  }

  static getChildData(req, res) {
    res.render("pages/data-anak.ejs");
  }

  static getOfferDetail(req, res) {
    res.render("pages/detail-penawaran.ejs");
  }
  static getLatestOffer(req, res) {
    res.render("pages/penawaran-terakhir.ejs");
  }
  static getTestFE(req, res) {
    res.render("tutorditerima.ejs");
  }
  static logout(req, res) {
    req.logout();
    res.redirect("/");
  }
}

module.exports = Auth;
