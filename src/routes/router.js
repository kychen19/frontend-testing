const express = require("express");
const router = express.Router();
const material = require("../controllers/materialController");
const subject = require("../controllers/subjectController");
const controller = require("../controllers/authController");
const children = require("../controllers/childCintroller");
const tracking = require("../controllers/trackingController");
const tutor = require("../controllers/tutorController");
const studentcont = require("../controllers/studentController");

var checkAuthentication = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect("/login");
  }
};
//auth Routing
router.post("/register", controller.register);
router.post("/login", controller.login);

router.get("/", controller.getHomepage);

// parents section
router.post("/child-data", checkAuthentication, children.create); 
router.get("/service", controller.getLatestOffer);
router.get("/parents-dashboard", checkAuthentication, children.getchild);
router.get("/child-data", checkAuthentication, controller.getChildData);
router.get("/latest-offer", controller.getLatestOffer);
router.get("/offer-detail", controller.getOfferDetail);
router.get("/child-report", controller.getBehaviouralDataReport);
router.get("/material", material.index);
router.get("/register", controller.getregister);
// router.post("/login", controller.login);
router.get("/login", controller.getLoginPage);
// router.get("/home", controller.getregister);

router.get("/tracking", tracking.getLaporan);

router.get("/subject/", subject.index);
router.post("/subject/create", subject.create);
router.patch("/subject/:id", subject.update);
router.delete("/subject/:id", subject.delete);

router.get("/test", controller.getTestFE);
// router.get("/student", student.index);
// router.post("/student/create", student.create);
// router.put("/student/update/:id", student.update);
// router.delete("/student/delete/:id", student.delete);

router.get("/tutor-dashboard", checkAuthentication, studentcont.dashboardTutor);
router.get("/student/:id", checkAuthentication, studentcont.studentDetail);
router.post("/addlaporan", studentcont.createLaporan);
router.get("/addlaporan", studentcont.getaddLaporan);
router.get("/listlaporan/:id", studentcont.getLaporan);

router.get("/tutor-dashboard/penawaran", tutor.getTutorOffer);
router.get("/tutor-dashboard/profile", tutor.getDataTutor);

module.exports = router;
