# Get All Tutor API

Fitur ini digunakan untuk menampilkan seluruh daftar data Tutor.
**URI Pattern**:

```
localhost:3000/allTutor
```

**Request Requirements**:

- Menampilkan keseluruhan daftar data Tutor.
  Berikut adalah contoh sebuah request yang valid :

```
var settings = {
  "url": "localhost:3000/allTutor",
  "method": "GET",
  "timeout": 0,
};

$.ajax(settings).done(function (response) {
  console.log(response);
});

```

**Response**:
_Response_ yang diberikan dalam bentuk format JSON dengan ketentuan :

- Jika data yang dimasukkan valid maka akan mengembalikan status 200 dan pesan "found data", dengan informasi berupa: id, subjectId, userId, name, rating, informasi user login yang sesuai userId, dan lainnya.
- Berikut ini adalah contoh response yang diberikan ketika data yang dimasukkan valid.

```
{
    "status": 200,
    "data": [
        {
            "id": 1,
            "subjectId": 1,
            "userId": 2,
            "rating": 5,
            "name": "Adi",
            "createdAt": "2021-08-10T17:09:53.709Z",
            "updatedAt": "2021-08-10T17:09:53.709Z",
            "user": {
                "id": 2,
                "username": "tutor1",
                "email": "t1@t1.com",
                "password": "$2b$10$S9glYEeTGUZFahSaRXWWj.d/RQpcSRI7jF1b/ht6GqvcsMcj/0cXG",
                "fullname": null,
                "jeniskelamin": null,
                "provinsi": null,
                "gaji": null,
                "deskripsi": null,
                "phonenumber": null,
                "address": null,
                "role": "TUTOR",
                "createdAt": "2021-08-10T17:09:53.485Z",
                "updatedAt": "2021-08-10T17:09:53.485Z"
            }
        },
        {
            "id": 2,
            "subjectId": 2,
            "userId": 2,
            "rating": 5,
            "name": "Wati",
            "createdAt": "2021-08-10T17:09:53.709Z",
            "updatedAt": "2021-08-10T17:09:53.709Z",
            "user": {
                "id": 2,
                "username": "tutor1",
                "email": "t1@t1.com",
                "password": "$2b$10$S9glYEeTGUZFahSaRXWWj.d/RQpcSRI7jF1b/ht6GqvcsMcj/0cXG",
                "fullname": null,
                "jeniskelamin": null,
                "provinsi": null,
                "gaji": null,
                "deskripsi": null,
                "phonenumber": null,
                "address": null,
                "role": "TUTOR",
                "createdAt": "2021-08-10T17:09:53.485Z",
                "updatedAt": "2021-08-10T17:09:53.485Z"
            }
        },
        {
            "id": 3,
            "subjectId": 2,
            "userId": 2,
            "rating": 5,
            "name": "Wawan",
            "createdAt": "2021-08-10T17:09:53.709Z",
            "updatedAt": "2021-08-10T17:09:53.709Z",
            "user": {
                "id": 2,
                "username": "tutor1",
                "email": "t1@t1.com",
                "password": "$2b$10$S9glYEeTGUZFahSaRXWWj.d/RQpcSRI7jF1b/ht6GqvcsMcj/0cXG",
                "fullname": null,
                "jeniskelamin": null,
                "provinsi": null,
                "gaji": null,
                "deskripsi": null,
                "phonenumber": null,
                "address": null,
                "role": "TUTOR",
                "createdAt": "2021-08-10T17:09:53.485Z",
                "updatedAt": "2021-08-10T17:09:53.485Z"
            }
        },
        {
            "id": 4,
            "subjectId": 2,
            "userId": 2,
            "rating": 5,
            "name": "Bambang",
            "createdAt": "2021-08-10T17:09:53.709Z",
            "updatedAt": "2021-08-10T17:09:53.709Z",
            "user": {
                "id": 2,
                "username": "tutor1",
                "email": "t1@t1.com",
                "password": "$2b$10$S9glYEeTGUZFahSaRXWWj.d/RQpcSRI7jF1b/ht6GqvcsMcj/0cXG",
                "fullname": null,
                "jeniskelamin": null,
                "provinsi": null,
                "gaji": null,
                "deskripsi": null,
                "phonenumber": null,
                "address": null,
                "role": "TUTOR",
                "createdAt": "2021-08-10T17:09:53.485Z",
                "updatedAt": "2021-08-10T17:09:53.485Z"
            }
        }
    ],
    "message": "found data"
}
```
