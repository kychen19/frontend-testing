# Create Service API

Fitur ini digunakan untuk menambahkan data service Tutor yang dilakukan oleh user Tutor.
**URI Pattern**:

```
localhost:3000/search
```

**Request Requirements**:

- Setiap data service yang akan ditambahkan, harus sesuai dengan format yang telah ditentukan yaitu subjectId, userId, name, rating.
  Berikut adalah contoh sebuah request yang valid :

```
var settings = {
  "url": "localhost:3000/search",
  "method": "POST",
  "timeout": 0,
  "data": {
    "id": 19,
    "subjectId": 1,
    "userId": 1,
    "rating": 4,
    "name": "Bambang",
    "updatedAt": "2021-08-10T13:23:24.508Z",
    "createdAt": "2021-08-10T13:23:24.508Z"
  }
};

$.ajax(settings).done(function (response) {
  console.log(response);
});

```

**Response**:
_Response_ yang diberikan dalam bentuk format JSON dengan ketentuan :

- Jika data yang dimasukkan valid maka akan mengembalikan status 200 dan pesan "data service created", dengan informasi berupa: id, subjectId, userId, name, rating, dan lainnya.
- Berikut ini adalah contoh response yang diberikan ketika data yang dimasukkan valid.

```
{
    "status": 200,
    "data": {
        "id": 19,
        "subjectId": 1,
        "userId": 1,
        "rating": 4,
        "name": "Bambang",
        "updatedAt": "2021-08-10T13:23:24.508Z",
        "createdAt": "2021-08-10T13:23:24.508Z"
    },
    "message": "data service created"
}
```
